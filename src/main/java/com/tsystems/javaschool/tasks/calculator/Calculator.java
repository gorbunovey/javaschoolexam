package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        // Null,empty check:
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        // Make some preparations
        // Replace unary operator '-' to binary:
        String expression = statement.replaceAll("\\(-", "(0-")
                .replaceAll("\\)-", "\\)0-");

        // Check for unary operator '-' on first place:
        if (expression.charAt(0) == '-') {
            expression = "0" + expression;
        }

        // Replace all spaces (optional - no info in task):
        expression = expression.replaceAll("\\s+", "");
        // And additional check if was only spaces:
        if (expression.isEmpty()) {
            return null;
        }

        // Split expression into tokens array:
        String[] expressionTokens = expression.split("((?<=[+\\-*/()])|(?=[+\\-*/()]))");

        // For calculations using 2 stacks: for numbers and for operations
        Stack<String> operationStack = new Stack<String>();
        Stack<Double> operandStack = new Stack<Double>();

        // Inappropriate order of operations and numbers will cause exceptions
        try {
            for (int i = 0; i < expressionTokens.length; i++) {
                String token = expressionTokens[i];
                if (isOpenBracket(token)) {
                    operationStack.push(token);
                } else if (isCloseBracket(token)) {
                    while (true) {
                        String lastOperator = operationStack.peek(); // throws EmptyStackException
                        if (isOpenBracket(lastOperator)) {
                            operationStack.pop();
                            break;
                        } else if (isOperator(lastOperator)) {
                            double op2 = operandStack.pop(); // throws EmptyStackException
                            double op1 = operandStack.pop(); // throws EmptyStackException
                            operandStack.push(makeOperation(op1, op2, operationStack.pop())); // throws IllegalArgumentException
                        } else {
                            throw new IllegalArgumentException();
                        }
                    }
                } else if (isNumber(token)) {
                    Double tmp = Double.parseDouble(token); // throws NullPointerException|NumberFormatException
                    operandStack.push(tmp);
                } else if (isOperator(token)) {
                    while (true) {
                        if (operationStack.empty()) {
                            operationStack.push(token);
                            break;
                        } else {
                            String lastOperator = operationStack.peek(); // throws EmptyStackException
                            if (isOpenBracket(lastOperator) || getPriority(lastOperator) < getPriority(token)) {
                                operationStack.push(token);
                                break;
                            } else {
                                double op2 = operandStack.pop(); // throws EmptyStackException
                                double op1 = operandStack.pop(); // throws EmptyStackException
                                operandStack.push(makeOperation(op1, op2, operationStack.pop())); // throws IllegalArgumentException
                            }
                        }
                    }

                } else {
                    return null; // incorrect token
                }

            }

            // Calculate remaining in stacks:
            while (!operationStack.empty()) {
                double op2 = operandStack.pop(); // throws EmptyStackException
                double op1 = operandStack.pop(); // throws EmptyStackException
                operandStack.push(makeOperation(op1, op2, operationStack.pop())); // throws IllegalArgumentException
            }

            // Formatting the result:
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');
            NumberFormat formatter = new DecimalFormat("#0.####", otherSymbols);
            return formatter.format(operandStack.pop()); // throws EmptyStackException

        } catch (EmptyStackException | IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }

    // List of available operators:
    private static final String OPERATORS = "+-*/";

    // Helper methods:
    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (NullPointerException | NumberFormatException e) {
            return false;
        }

    }

    private double makeOperation(double op1, double op2, String operation) throws IllegalArgumentException {
        switch (operation) {
            case "+":
                return op1 + op2;
            case "-":
                return op1 - op2;
            case "*":
                return op1 * op2;
            case "/":
                if(op2 != 0){
                    return op1 / op2;
                }
            default:
                throw new IllegalArgumentException("doesn't support this operation");
        }
    }

    private int getPriority(String operator) throws IllegalArgumentException {
        switch (operator) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                throw new IllegalArgumentException("doesn't support this operator");
        }
    }
}
