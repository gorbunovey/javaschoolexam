package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.TreeSet;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // List sanity check:
        //if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null) || inputNumbers.contains(new Integer(null))){
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }
        // Ordering list elements:
        TreeSet<Integer> numbersHeapTree = new TreeSet<>(inputNumbers);

        // Check for minimal number of elements:
        if(numbersHeapTree.size() < 3){
            throw new CannotBuildPyramidException();
        }

        // Check if there are enough numbers for the pyramid:
        int numbersHeap = numbersHeapTree.size();
        int pyramidLevel = 0;
        while(numbersHeap > 0){
            pyramidLevel++;
            numbersHeap-=pyramidLevel;
            if(numbersHeap < 0){
                throw new CannotBuildPyramidException();
            }
        }
        // Build the pyramid:
        int pyramidWidth = 2*pyramidLevel-1;
        int[][] pyramid = new int[pyramidLevel][pyramidWidth];
        for (int i = 0; i < pyramidLevel; i++) {
            int shift = pyramidLevel - 1 - i;
            for (int j = shift; j < pyramidWidth - shift; j+=2) {
                pyramid[i][j] = numbersHeapTree.pollFirst();
            }
        }

        // TODO : Implement your solution here
        return pyramid;
    }


}
