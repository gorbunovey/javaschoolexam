package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // Check nulls:
        if(x == null || y == null)
        {
            throw new IllegalArgumentException("null  is not acceptable argument");
        }
        // Handle empty sequences:
        if (x.isEmpty()) return true;
        if (y.isEmpty()) return false;
        // Handle non empty sequences:
        // Run cycle on y(longer) sequence and look for first element from x(shorter) sequence
        // if find - increase index and look for next
        // if index reached size of x sequence - then we found subsequence = true
        // if sequence y ended earlier = false
        int index = 0;
        Object xObject = x.get(index);
        for (Object yObject:y) {
            if(yObject.equals(xObject)){
                if(index+1 >= x.size()){
                    return true;
                }else{
                    index++;
                    xObject = x.get(index);
                }
            }
        }
        return false;
    }
}
